import 'package:ejemplo_login/blocs/auth/bloc.dart';
import 'package:ejemplo_login/blocs/auth_providers/auth_providers_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'login_button.dart';

class AuthProviders extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthProvidersBloc, AuthProvidersState>(
      listener: (BuildContext context, AuthProvidersState state) {
        if (state is AuthProvidersFailure) {
          /* showSnackBar(
            context,
            msg: state.msg,
            type: SnackBarType.error,
          ); */
          print("error");
        } else if (state is AuthProvidersSuccess) {
          BlocProvider.of<AuthBloc>(context).add(AuthenticatedIn());
        }
      },
      child: BlocBuilder<AuthProvidersBloc, AuthProvidersState>(
        builder: (BuildContext context, AuthProvidersState state) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Center(child: Text("Logo")),
                ),
                LoginButton(
                  buttonType: ButtonType.Facebook,
                  onPressed: (state is AuthProvidersLoading)
                      ? null
                      : () {
                          BlocProvider.of<AuthProvidersBloc>(context).add(
                            FacebookButtonPressed(),
                          );
                        },
                ),
                Padding(
                  padding: EdgeInsets.all(10),
                ),
                LoginButton(
                  buttonType: ButtonType.Google,
                  onPressed: (state is AuthProvidersLoading)
                      ? null
                      : () {
                          BlocProvider.of<AuthProvidersBloc>(context).add(
                            GoogleButtonPressed(),
                          );
                        },
                ),
                Padding(
                  padding: EdgeInsets.all(5),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
