import 'package:flutter/material.dart';

class FatalErrorUI extends StatelessWidget {
  final String msg;
  final bool tryAgain;

  const FatalErrorUI({
    Key key,
    this.msg,
    this.tryAgain = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 30.0),
              child: Icon(
                Icons.error,
                color: Theme.of(context).errorColor,
                size: 70,
              ),
            ),
            Text(
              'Opps... Algo salió mal',
              style: Theme.of(context).textTheme.display1,
              overflow: TextOverflow.ellipsis,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
              child: Text(
                getMessage(),
                style: Theme.of(context).textTheme.title.apply(
                      color: Theme.of(context).disabledColor,
                    ),
                textAlign: TextAlign.center,
                maxLines: 4,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        ),
      ),
    );
  }

  String getMessage() {
    String message = "";
    if (msg != null) message += msg;
    if (tryAgain) {
      if (msg != null) message += ",";
      message += " intentalo más tarde.";
    }
    return message;
  }
}
