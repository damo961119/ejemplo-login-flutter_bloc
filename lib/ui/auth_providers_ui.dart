import 'package:ejemplo_login/blocs/auth_providers/auth_providers_bloc.dart';
import 'package:ejemplo_login/repo/auth_repo.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'auth_providers.dart';

class AuthProvidersUI extends StatelessWidget {
  final AuthRepository authRepository;

  const AuthProvidersUI({
    Key key,
    @required this.authRepository,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Theme.of(context).primaryColor,
      body: BlocProvider<AuthProvidersBloc>(
        create: (context) => AuthProvidersBloc(authRepository: authRepository),
        child: AuthProviders(),
      ),
    );
  }
}
