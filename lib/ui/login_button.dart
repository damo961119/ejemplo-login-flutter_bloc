import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

enum ButtonType { Facebook, Google }

class LoginButton extends StatelessWidget {
  final ButtonType buttonType;
  final VoidCallback onPressed;

  const LoginButton({
    Key key,
    this.buttonType = ButtonType.Google,
    @required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 52,
      width: double.infinity,
      child: (buttonType == ButtonType.Facebook)
          ? RaisedButton.icon(
              color: const Color(0xFF3b5998),
              icon: Icon(
                FontAwesomeIcons.facebookSquare,
                color: Colors.white,
              ),
              label: Text(
                "Ingresar con Facebook",
                style: Theme.of(context).textTheme.button.copyWith(
                      fontSize: 17,
                      color: Colors.white,
                    ),
              ),
              onPressed: onPressed,
            )
          : Container(
              color: Colors.white,
              child: OutlineButton.icon(
                icon: Icon(
                  FontAwesomeIcons.google,
                ),
                label: Text(
                  "Ingresar con Google",
                  style: Theme.of(context).textTheme.button.copyWith(
                        fontSize: 17,
                        color: Theme.of(context).primaryColor,
                      ),
                ),
                onPressed: onPressed,
              ),
            ),
    );
  }
}
