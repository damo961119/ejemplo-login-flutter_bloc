import 'package:cloud_firestore/cloud_firestore.dart'
    show DocumentSnapshot, FieldValue;
import 'package:firebase_auth/firebase_auth.dart' show FirebaseUser;

class User {
  String uid;
  String name;
  String phone;
  String email;

  dynamic createdAt;
  dynamic updatedAt;

  User({
    this.uid,
    this.name,
    this.phone,
    this.email,
    this.createdAt,
    this.updatedAt,
  });

  factory User.fromFirebaseUser(FirebaseUser user) => User(
        uid: user.uid,
        phone: user.phoneNumber,
        email: user.email,
      );

  factory User.fromDocumentSnapshot(DocumentSnapshot documentSnapshot) {
    User user = User.fromJson(documentSnapshot.data);
    user.uid = documentSnapshot.documentID;
    return user;
  }

  factory User.fromJson(Map<String, dynamic> json) => User(
        uid: json["uid"] == null ? null : json["uid"],
        name: json["name"] == null ? null : json["name"],
        phone: json["phone"] == null ? null : json["phone"],
        email: json["email"] == null ? null : json["email"],
        createdAt: json["createdAt"] == null ? null : json["createdAt"],
        updatedAt: json["updatedAt"] == null ? null : json["updatedAt"],
      );

  Map<String, dynamic> toJson() => {
        "uid": uid == null ? null : uid,
        "name": name == null ? null : name,
        "phone": phone == null ? null : phone,
        "email": email == null ? null : email,
        "createdAt": createdAt == null ? null : createdAt,
        "updatedAt": updatedAt == null ? null : updatedAt,
      };

  void setCreateAt() => createdAt = FieldValue.serverTimestamp();

  void setUpdatedAt() => updatedAt = FieldValue.serverTimestamp();

  @override
  String toString() => "${toJson()}";
}
