import 'package:cloud_firestore/cloud_firestore.dart';
import '../models/user.dart';

class FirestoreUserProvider {
  final CollectionReference userCollection =
      Firestore.instance.collection("users");

  Future<User> getUser(String uid) async {
    DocumentSnapshot userDocument = await userCollection.document(uid).get();
    if (!userDocument.exists) return null;
    return User.fromDocumentSnapshot(userDocument);
  }

  Future<void> setUser(User user) async {
    return await userCollection.document(user.uid).setData(user.toJson());
  }
}
