import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';

import '../models/user.dart';
import 'firestore_user.dart';

class AuthRepository {
  final FirebaseAuth _auth;
  final GoogleSignIn _googleSignIn;
  final FacebookLogin _facebookLogin;
  final FirestoreUserProvider _firestoreUserProvider;

  AuthRepository({
    FirebaseAuth auth,
    GoogleSignIn googleSignin,
    FacebookLogin facebookLogin,
    FirestoreUserProvider firestoreUserProvider,
  })  : _auth = auth ?? FirebaseAuth.instance,
        _googleSignIn = googleSignin ?? GoogleSignIn(),
        _facebookLogin = facebookLogin ?? FacebookLogin(),
        _firestoreUserProvider =
            firestoreUserProvider ?? FirestoreUserProvider();

  Future<User> getCurrentUser() async {
    final FirebaseUser firebaseUser = await _auth.currentUser();
    if (firebaseUser == null) return null;
    return User.fromFirebaseUser(firebaseUser);
  }

  Future<User> getUser(String uid) async {
    return await _firestoreUserProvider.getUser(uid);
  }

  Future<AuthCredential> signInWithGoogle() async {
    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    if (googleUser == null) return null;

    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    return credential;
  }

  Future<AuthCredential> signInWithFacebook() async {
    final facebookLoginResult = await _facebookLogin.logIn(['email']);
    switch (facebookLoginResult.status) {
      case FacebookLoginStatus.cancelledByUser:
        return null;
      case FacebookLoginStatus.error:
        throw facebookLoginResult.errorMessage;
      case FacebookLoginStatus.loggedIn:
        break;
    }

    final accessToken = facebookLoginResult.accessToken.token;
    final facebookAuthCred = FacebookAuthProvider.getCredential(
      accessToken: accessToken,
    );

    return facebookAuthCred;
  }

  Future<void> sendSmsCodeToPhone(
    String phoneNumber,
    PhoneCodeSent codeSent,
    PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout,
    PhoneVerificationFailed onFailed,
    PhoneVerificationCompleted onCompleted,
  ) async {
    await _auth.verifyPhoneNumber(
      timeout: const Duration(minutes: 1),
      phoneNumber: phoneNumber,
      codeSent: codeSent,
      codeAutoRetrievalTimeout: codeAutoRetrievalTimeout,
      verificationFailed: onFailed,
      verificationCompleted: onCompleted,
    );
  }

  Future<void> signInWithCredential(AuthCredential authCredential) async {
    await _auth.signInWithCredential(authCredential);
  }

  Future<void> setUser(User user) async {
    user.setCreateAt();
    user.setUpdatedAt();
    return await _firestoreUserProvider.setUser(user);
  }

  Future<void> singOutFbAndGoogle() async {
    return await Future.wait([
      _googleSignIn.signOut(),
      _facebookLogin.logOut(),
    ]);
  }

  Future<void> singOut() async {
    return await Future.wait([
      _auth.signOut(),
      singOutFbAndGoogle(),
    ]);
  }
}
