import 'package:ejemplo_login/repo/auth_repo.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'navigation/routes.dart' as routes;
import 'blocs/auth/bloc.dart';
import 'navigation/routing_constants.dart';

void main() {
  final authRepository = AuthRepository();
  runApp(
    RepositoryProvider<AuthRepository>(
      create: (context) => authRepository,
      child: BlocProvider<AuthBloc>(
        create: (context) => AuthBloc(
          authRepository: authRepository,
        )..add(AppStarted()),
        child: MaterialApp(
          title: "APP",
          routes: routes.mainRoute(),
          initialRoute: MainRoute,
        ),
      ),
    ),
  );
}

class Hello extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text("Hello"),
    );
  }
}
