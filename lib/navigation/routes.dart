import 'package:ejemplo_login/blocs/auth/bloc.dart';
import 'package:ejemplo_login/repo/auth_repo.dart';
import 'package:ejemplo_login/ui/auth_providers_ui.dart';
import 'package:ejemplo_login/ui/fatal_error_ui.dart';
import 'package:ejemplo_login/ui/splash_ui.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import './routing_constants.dart';

Map<String, Widget Function(BuildContext)> mainRoute() {
  return {
    MainRoute: (context) {
      return BlocBuilder<AuthBloc, AuthState>(
        builder: (BuildContext context, AuthState authState) {
          if (authState is AuthLoading) {
            return SplashUI();
          } else if (authState is Unauthenticated) {
            return AuthProvidersUI(
              authRepository: RepositoryProvider.of<AuthRepository>(context),
            );
          } else if (authState is Authenticated) {
            return Scaffold(
              body: Center(
                child: RaisedButton(
                  onPressed: () {
                    BlocProvider.of<AuthBloc>(context).add(LoggedOut());
                  },
                  child: Text("Logout"),
                ),
              ),
            );
          } else if (authState is AuthError) {
            return FatalErrorUI(
              msg: authState.msg,
            );
          }
          return FatalErrorUI();
        },
      );
    }
  };
}
