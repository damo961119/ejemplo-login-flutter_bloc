const String MainRoute = '/';
const String PhoneAuthRoute = 'phoneAuth';
const String ConfirmCodeRoute = 'confirmCode';
const String PromotionsMethodRoute = 'promotionMethod';

const String WebViewRoute = 'webview';
const String PaymentInfoRoute = 'paymentInfo';
const String PaymentStatusRoute = 'paymentStatus';
