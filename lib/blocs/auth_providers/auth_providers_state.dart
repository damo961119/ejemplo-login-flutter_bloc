part of 'auth_providers_bloc.dart';

abstract class AuthProvidersState extends Equatable {
  const AuthProvidersState();
}

class AuthProvidersInitial extends AuthProvidersState {
  @override
  List<Object> get props => [];
  @override
  String toString() => "AuthProvidersInitial";
}

class AuthProvidersLoading extends AuthProvidersState {
  @override
  String toString() => "AuthProvidersLoading";
  @override
  List<Object> get props => null;
}

class AuthProvidersFailure extends AuthProvidersState {
  final String msg;
  const AuthProvidersFailure(this.msg);

  @override
  String toString() => "AuthProvidersFailure{msg:$msg}";
  @override
  List<Object> get props => [msg];
}

class AuthProvidersSuccess extends AuthProvidersState {
  @override
  String toString() => "AuthProvidersSuccess";
  @override
  List<Object> get props => [];
}
