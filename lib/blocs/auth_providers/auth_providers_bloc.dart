import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import '../../repo/auth_repo.dart';

part 'auth_providers_event.dart';
part 'auth_providers_state.dart';

class AuthProvidersBloc extends Bloc<AuthProvidersEvent, AuthProvidersState> {
  final AuthRepository authRepository;

  AuthProvidersBloc({
    @required this.authRepository,
  }) : assert(authRepository != null);
  @override
  AuthProvidersState get initialState => AuthProvidersInitial();

  @override
  Stream<AuthProvidersState> mapEventToState(
    AuthProvidersEvent event,
  ) async* {
    if (event is GoogleButtonPressed) {
      try {
        yield AuthProvidersLoading();
        final authCredential = await authRepository.signInWithGoogle();
        if (authCredential == null) {
          yield AuthProvidersFailure("Opps! no pudimos realizar la acción");
          return;
        }
        await authRepository.signInWithCredential(authCredential);
        final firebaseUser = await authRepository.getCurrentUser();

        await authRepository.setUser(firebaseUser);
        yield AuthProvidersSuccess();
      } catch (e) {
        yield AuthProvidersFailure("$e");
      }
    } else if (event is FacebookButtonPressed) {
      try {
        yield AuthProvidersLoading();
        final authCredential = await authRepository.signInWithFacebook();
        if (authCredential == null) {
          yield AuthProvidersFailure("Opps! no pudimos realizar la acción");
          return;
        }
        await authRepository.signInWithCredential(authCredential);

        final firebaseUser = await authRepository.getCurrentUser();

        await authRepository.setUser(firebaseUser);
        yield AuthProvidersSuccess();
      } catch (e) {
        yield AuthProvidersFailure("$e");
      }
    }
  }
}
