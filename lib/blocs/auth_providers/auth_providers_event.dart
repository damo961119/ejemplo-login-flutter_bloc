part of 'auth_providers_bloc.dart';

abstract class AuthProvidersEvent extends Equatable {
  const AuthProvidersEvent();
}

class GoogleButtonPressed extends AuthProvidersEvent {
  @override
  String toString() => 'GoogleButtonPressed';

  @override
  List<Object> get props => [];
}

class FacebookButtonPressed extends AuthProvidersEvent {
  @override
  String toString() => 'FacebookButtonPressed';

  @override
  List<Object> get props => [];
}
