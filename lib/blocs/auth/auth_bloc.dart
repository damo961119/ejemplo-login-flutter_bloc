import 'dart:async';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';

import '../../repo/auth_repo.dart';
import '../../models/user.dart';
import './bloc.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthRepository authRepository;

  AuthBloc({
    @required this.authRepository,
  }) : assert(authRepository != null);

  @override
  AuthState get initialState => AuthLoading();

  @override
  Stream<AuthState> mapEventToState(
    AuthEvent event,
  ) async* {
    if (event is AppStarted) {
      yield* mapAppStartedToState();
    } else if (event is AuthenticatedIn) {
      yield* mapAuthenticatedInToState();
    } else if (event is LoggedOut) {
      yield* mapLoggedOutToState();
    }
  }

  Stream<AuthState> mapAppStartedToState() async* {
    yield AuthLoading();
    try {
      yield await getCurrentAuthState();
    } catch (e) {
      yield AuthError("$e");
    }
  }

  Stream<AuthState> mapAuthenticatedInToState() async* {
    try {
      yield AuthLoading();
      yield await getCurrentAuthState();
    } catch (e) {
      yield AuthError("$e");
    }
  }

  Future<AuthState> getCurrentAuthState() async {
    final User firebaseUser = await authRepository.getCurrentUser();
    if (firebaseUser == null) return Unauthenticated();

    final User user = await authRepository.getUser(firebaseUser.uid);

    if (user == null) return Unauthenticated();

    return Authenticated(user);
  }

  Stream<AuthState> mapLoggedOutToState() async* {
    yield AuthLoading();
    try {
      await authRepository.singOut();
      yield Unauthenticated();
    } catch (e) {
      yield AuthError("");
    }
  }

  @override
  String toString() => "AuthBloc";
}
