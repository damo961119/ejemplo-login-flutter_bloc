import 'package:equatable/equatable.dart';
import '../../models/user.dart';

abstract class AuthState extends Equatable {
  const AuthState();
}

class AuthLoading extends AuthState {
  @override
  String toString() => 'AuthLoading';

  @override
  List<Object> get props => null;
}

class Unauthenticated extends AuthState {
  @override
  String toString() => 'Unauthenticated';

  @override
  List<Object> get props => null;
}

class Authenticated extends AuthState {
  final User user;

  const Authenticated(this.user);

  @override
  String toString() => 'Authenticated{user:$user}';

  @override
  List<Object> get props => [user];
}

class AuthError extends AuthState {
  final String msg;

  const AuthError(this.msg);

  @override
  String toString() => 'AuthError{error:$msg}';

  @override
  List<Object> get props => [msg];
}
